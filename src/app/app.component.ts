import {Component, OnInit} from '@angular/core';
import {ModalService} from './services/modal.service';
import {ModalContentComponent} from './modal-content/modal-content.component';
import {DropDownItem} from './interfaces/drop-down-item';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [ModalService]
})
export class AppComponent {
  public items: DropDownItem[] = [
    {value: 'foo', label: 'Fooo'},
    {value: 'boo', label: 'Booo'},
    {value: 'moo', label: 'Mooo'},
  ];
  constructor(private modalService: ModalService) {
  }

  public openOverlay(): void {
    this.modalService.createModal<ModalContentComponent>(ModalContentComponent);
  }
}
