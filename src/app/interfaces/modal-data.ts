import {InjectionToken} from '@angular/core';

export const MODAL_DATA: InjectionToken<any> = new InjectionToken('ModalData');
