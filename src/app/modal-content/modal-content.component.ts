import {Component, Inject, OnInit, Optional} from '@angular/core';
import {OverlayRef} from '@angular/cdk/overlay';
import {MODAL_DATA} from '../interfaces/modal-data';

@Component({
  selector: 'app-modal-content',
  templateUrl: './modal-content.component.html',
  styleUrls: ['./modal-content.component.scss']
})
export class ModalContentComponent implements OnInit {

  constructor(
    @Optional() @Inject(MODAL_DATA) public modalData: any,
    private overlayRef: OverlayRef
  ) { }

  ngOnInit() {
    console.log(this.modalData);
  }
  public close(): void {
    this.overlayRef.detach();
  }
}
