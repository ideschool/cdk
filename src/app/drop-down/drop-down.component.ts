import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {DropDownItem} from '../interfaces/drop-down-item';
import {CdkOverlayOrigin, ConnectedPosition, Overlay} from '@angular/cdk/overlay';

@Component({
  selector: 'app-drop-down',
  templateUrl: './drop-down.component.html',
  styleUrls: ['./drop-down.component.scss']
})
export class DropDownComponent {
  @Input() public placeholder: string;
  @Input() public items: DropDownItem[] = [];
  public openDrawer: boolean = false;
  public value: DropDownItem;
  public positions: ConnectedPosition[] = [
    {
      originX: 'start',
      overlayX: 'start',
      originY: 'bottom',
      overlayY: 'top'
    },
    {
      originX: 'start',
      overlayX: 'start',
      originY: 'top',
      overlayY: 'bottom'
    }
  ];

  public select(item: DropDownItem) {
    this.value = item;
    this.openDrawer = false;
  }
}
