import {Component, OnInit, ViewChild} from '@angular/core';
import {CdkVirtualScrollViewport} from '@angular/cdk/scrolling';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  public items: string[] = [];
  public index: number = 0;

  @ViewChild(CdkVirtualScrollViewport, {static: true}) public scrollViewport: CdkVirtualScrollViewport;

  constructor() {
  }

  public ngOnInit(): void {
    for (let i = 0; i < 1000; i++) {
      this.items.push(`ITEM ${i + 1}`);
    }
  }

  public scroll() {
    this.scrollViewport.scrollToIndex(this.index);
  }
}
