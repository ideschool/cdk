import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ModalContentComponent } from './modal-content/modal-content.component';
import {OverlayModule} from '@angular/cdk/overlay';
import {PortalModule} from '@angular/cdk/portal';
import { DropDownComponent } from './drop-down/drop-down.component';
import { ListComponent } from './list/list.component';
import {ScrollingModule} from '@angular/cdk/scrolling';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    ModalContentComponent,
    DropDownComponent,
    ListComponent,
  ],
  imports: [
    BrowserModule,
    OverlayModule,
    PortalModule,
    ScrollingModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [ModalContentComponent],
})
export class AppModule { }
