import {Injectable, InjectionToken, Injector} from '@angular/core';
import {ComponentType, Overlay, OverlayConfig, OverlayRef, PositionStrategy} from '@angular/cdk/overlay';
import {ComponentPortal, PortalInjector} from '@angular/cdk/portal';
import {MODAL_DATA} from '../interfaces/modal-data';

@Injectable({
  providedIn: 'root'
})
export class ModalService {
  constructor(private overlay: Overlay, private injector: Injector) {
  }

  public overlayRef: OverlayRef;

  public closeModal(): void {
    if (this.overlayRef) {
      this.overlayRef.detach();
    }
  }

  public createModal<T>(component: ComponentType<T>, overlayConfig?: OverlayConfig, data?: any): void {
    const config: OverlayConfig = this._overlayConfig(overlayConfig);
    this.overlayRef = this.overlay.create(config);
    const injector: PortalInjector = this._createInjector(data);
    const modalContent: ComponentPortal<T> = new ComponentPortal<T>(component, null, injector);
    this.overlayRef.attach<T>(modalContent);
    this.overlayRef.backdropClick().subscribe(this.closeModal.bind(this));
  }

  private _overlayConfig(config?: OverlayConfig): OverlayConfig {
    const positionStrategy: PositionStrategy = this.overlay.position().global()
      .centerVertically()
      .centerHorizontally();

    const defaultConfig: OverlayConfig = {
      hasBackdrop: true,
      backdropClass: 'dark-backdrop',
      panelClass: 'modal-content',
      maxWidth: 600,
      positionStrategy,
    };

    return {...defaultConfig, ...config};
  }

  private _createInjector(data?: any): PortalInjector {
    const tokens = new WeakMap();
    tokens.set(MODAL_DATA, data);
    tokens.set(OverlayRef, this.overlayRef);
    return new PortalInjector(this.injector, tokens);
  }
}
